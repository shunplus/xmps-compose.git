package com.szlanyou.compose.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val white = Color(0xFFFFFFFF)
val appThemColor = Color(0xFF48AF60)
val TextBlack = Color(0xFF2E3246)
val TextEdit = Color(0xFFB0B3BB)