package com.szlanyou.compose

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.statusBarsHeight
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.szlanyou.compose.ui.theme.TextBlack
import com.szlanyou.compose.ui.theme.TextEdit
import com.szlanyou.compose.ui.theme.XmpscomposeTheme
import com.szlanyou.compose.ui.theme.appThemColor
import com.szlanyou.compose.ui.theme.white

/**
 *
 * @ProjectName: Xpms-Compose
 * @Package: com.shun.compose.ui.user
 * @ClassName: LoginActivity
 * @Description:
 * @Author: shun.xu
 * @CreateDate: 2022/3/9 11:56 上午
 * @UpdateDate: 2022/3/9 11:56 上午
 * @Email: shunplus@163.com
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            Navigator()
        }
    }
}


/**
 * 页面导航
 *
 * startDestination = "Home"
 * startDestination = "Second"
 *
 * 如果本页只用做路由页面，不写布局,可以跳转到任意指定页面，如上述代码。
 *
 * 此处只用做导航，默认打开 Home 页面
 */
@Composable
fun Navigator() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "LoginPage") {
        composable("LoginPage") { LoginPage(navController) }
        composable("HomePage") { HomePage(navController) }
        composable("ListPage"){ ListPage(navController)}
        composable("MessagePage"){ MessagePage(navController) }
        composable("Mine"){ Mine(navController) }
    }
}


@Composable
fun LoginPage(navController: NavController) {

    //实例系统UI控制
    val systemUiController = rememberSystemUiController()
    //设置为透明状态栏
    systemUiController.setStatusBarColor(
        Color.Transparent, darkIcons = MaterialTheme.colors.isLight
    )

    XmpscomposeTheme {
        // A surface container using the 'background' color from the theme
        Surface(color = MaterialTheme.colors.background) {
            BodyView(navController)
        }
    }

}
@Composable
fun BodyView(navController: NavController) {
    var textMail by remember { mutableStateOf("") }
    var textPassWord by remember { mutableStateOf("") }
    var checked by rememberSaveable { mutableStateOf(false) }
    Column {
        Text(
            text = "欢迎来到Compose！",
            modifier = Modifier.padding(start = 18.dp, top = 75.dp),
            style = TextStyle(
                color = TextBlack,
                fontSize = 19.sp
            )
        )

        TextField(
            maxLines = 1,
            singleLine = true,
            value = textMail,
            onValueChange = {
                textMail = it
            },
            modifier = Modifier
                .padding(start = 18.dp, end = 18.dp, top = 40.dp)
                .fillMaxWidth()
                .background(
                    color = white,
                ),
            placeholder = {
                Text("请输入邮箱")
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email,
            ),
            textStyle = TextStyle(
                color = TextEdit,
//                fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                fontSize = 15.sp, textAlign = TextAlign.Start
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = white,  // 背景等相关颜色
                disabledIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = TextEdit,
                focusedIndicatorColor = TextEdit,
                errorIndicatorColor = Color.Red,
                placeholderColor = TextEdit,
                textColor = TextBlack,
                cursorColor = appThemColor
            ),
        )

        TextField(
            maxLines = 1,
            singleLine = true,
            value = textPassWord,
            onValueChange = {
                textPassWord = it
            },
            modifier = Modifier
                .padding(start = 18.dp, end = 18.dp, top = 10.dp)
                .fillMaxWidth()
                .background(
                    color = white,
                ),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
            ),
            placeholder = {
                Text("请输入密码")
            },
            textStyle = TextStyle(
                color = TextEdit,
//                fontFamily = FontFamily(Font(R.font.poppins_semibold)),
                fontSize = 15.sp, textAlign = TextAlign.Start
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = white,  // 背景等相关颜色
                disabledIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = TextEdit,
                focusedIndicatorColor = TextEdit,
                errorIndicatorColor = Color.Red,
                placeholderColor = TextEdit,
                textColor = TextBlack,
                cursorColor = appThemColor
            ),
        )

        Row {
            Checkbox(
                checked = checked,
                onCheckedChange = {
                    checked = it
                },
                modifier = Modifier.padding(start = 20.dp)
            )
            Text(
                text = "记住密码",
                modifier = Modifier.padding(top = 12.dp)
            )
        }


        Row {
            Button(
                onClick = {
                    navController.navigate("HomePage")
                },
                content = { Text(text = "登录") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 80.dp, end = 80.dp, top = 40.dp),
                shape = RoundedCornerShape(27.dp),//设置圆角
            )
        }


    }

}

@Composable
fun HomePage(navController: NavController) {
    XmpscomposeTheme {
        ProvideWindowInsets {
            rememberSystemUiController().setStatusBarColor(
                appThemColor, darkIcons = MaterialTheme.colors.isLight
            )

            Column {
                Spacer(
                    modifier = Modifier
                        .statusBarsHeight()
                        .fillMaxWidth()
                )

                var selectedItem by remember { mutableStateOf(1) }
                val navItems = listOf("消息", "主页", "我的")

                Scaffold(
                    topBar = {  // topBar 属性用于设置 AppBar
                        TopAppBar(
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentSize(Alignment.Center),
                            title = {  // 可设置标题
                                Text(
                                    text = "功能",
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .wrapContentSize(Alignment.Center),
                                )
                            },
                            actions = {  // 设置 AppBar 上的按钮 Button
                                IconButton(onClick = { /*TODO*/ }) {
                                    // Icon 系统为我们提供了许多常见的 Icon
                                    Icon(Icons.Filled.Favorite, contentDescription = null)
                                }
                            },
                            backgroundColor = appThemColor
                        )
                    },
                    bottomBar = {  // bottomBar 可用于设置 BottomNavigation
                        BottomNavigation(
                            backgroundColor = appThemColor
                        ) {
                            navItems.forEachIndexed { index, item ->
                                BottomNavigationItem(
                                    icon = { Icon(Icons.Filled.Face, contentDescription = null) },
                                    label = { Text(item) },
                                    selected = selectedItem == index,
                                    onClick = {
                                        selectedItem = index

                                    }
                                )
                            }
                        }
                    }
                ) {
                    when (selectedItem){
                        0->MessagePage( navController=navController)
                        1-> BodyContent(navController=navController)
                        2-> Mine(navController=navController)
                    }

                }

            }
        }
    }
}



@Composable
fun MessagePage(navController: NavController) {
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center) {
        Text(text = "这是消息")
    }
}

@Composable
fun Mine(navController: NavController) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(text = "这是我的")
    }

}

@Composable
fun BodyContent(navController: NavController) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Button(
            onClick = {
                navController.navigate("ListPage")
            },
            content = { Text(text = "列表") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 80.dp, end = 80.dp, top = 10.dp),
            shape = RoundedCornerShape(27.dp),//设置圆角
        )
        Button(
            onClick = {

            },
            content = { Text(text = "测试") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 80.dp, end = 80.dp, top = 10.dp),
            shape = RoundedCornerShape(27.dp),//设置圆角
        )
    }
}

@Composable
fun ListPage(navController: NavController) {
    XmpscomposeTheme {
        ProvideWindowInsets {
            rememberSystemUiController().setStatusBarColor(
                appThemColor, darkIcons = MaterialTheme.colors.isLight
            )

            Column {
                Spacer(
                    modifier = Modifier
                        .statusBarsHeight()
                        .fillMaxWidth()
                )

                Scaffold(
                    topBar = {  // topBar 属性用于设置 AppBar
                        TopAppBar(
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentSize(Alignment.Center),
                            title = {  // 可设置标题
                                Text(
                                    text = "列表",
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .wrapContentSize(Alignment.Center),
                                )
                            },
                            backgroundColor = appThemColor
                        )
                    },
                ) {
                    LazyColumn(
                        contentPadding = PaddingValues(horizontal = 16.dp, vertical =20.dp),
                    ){
                        // Add a single item
                        item {//添加一个
                            Text(text = "First item")
                        }

                        // Add 5 items
                        // 添加多个
                        items(5) { index ->
                            Text(text = "Item: $index",
                                modifier = Modifier.fillMaxWidth().padding(top = 20.dp)
                            )
                        }

                        // Add another single item
                        item {
                            Text(text = "Last item",
                                modifier = Modifier.fillMaxWidth().padding(top = 20.dp)
                            )
                        }
                    }

                }

            }
        }
    }
}

